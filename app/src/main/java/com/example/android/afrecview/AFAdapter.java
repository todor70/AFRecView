package com.example.android.afrecview;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class AFAdapter extends RecyclerView.Adapter<AFAdapter.ViewHolder>{


    private List<AndroidFlavor> itemList;


    public AFAdapter( List<AndroidFlavor> itemList) {

        this.itemList = itemList;

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);

        return new ViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.name.setText(itemList.get(position).getName());
        holder.number.setText(itemList.get(position).getNumber());
        holder.image.setImageResource(itemList.get(position).getImage());
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }


    public  class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{


        public TextView name;
        public TextView number;
        public ImageView image;

        public ViewHolder(View v){

            super(v);
            v.setOnClickListener(this);

            name = (TextView) itemView.findViewById(R.id.name);
            number = (TextView) itemView.findViewById(R.id.number);
            image = (ImageView) itemView.findViewById(R.id.image);
        }


        @Override
        public void onClick(View view) {
            Toast.makeText(view.getContext(), "Position = " + getAdapterPosition(), Toast.LENGTH_SHORT).show();
        }
    }




}
